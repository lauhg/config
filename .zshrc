# Path to your oh-my-zsh installation.
  export ZSH=~/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"
# ZSH_THEME="agnoster"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration

  export PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.local/bin:$HOME/racket/bin"
  export PATH=$PATH:/usr/local/go/bin:$HOME/gopath/bin:$HOME/.cargo/bin
  export PATH=$PATH:$HOME/.cabal/bin:$HOME/julia-3c9d75391c/bin
  export PATH=$PATH:$HOME/node-v6.10.0-linux-x64/bin:$HOME/Downloads/protoc-3.5.0-osx-x86_64/bin
  export PATH=$PATH:/usr/sbin:/sbin
  export PATH=$PATH:~/platform-tools:~/nand2tetris/tools
  export PATH=$PATH:~/dex2jar-2.0
  export GOPATH=$HOME/gopath
  export CARGO_HOME=$HOME/.cargo
  export RUST_SRC_PATH=$HOME/.rustup/toolchains/stable-i686-unknown-linux-gnu/lib/rustlib/src/rust/src
# export MANPATH="/usr/local/man:$MANPATH"
  export TZ='Asia/Shanghai'
  export CLASSPATH=".:/usr/local/lib/antlr-4.7-complete.jar:$CLASSPATH"
  alias antlr4='java -Xmx500M -cp "/usr/local/lib/antlr-4.7-complete.jar:$CLASSPATH" org.antlr.v4.Tool'
  alias grun='java org.antlr.v4.gui.TestRig'


export http_proxy=http://127.0.0.1:1087
export https_proxy=$http_proxy
export ftp_proxy=$http_proxy
export no_proxy=localhost

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
# alias cdp="cd \$(ls -a | percol)"
# export HISTCONTROL=ignoreboth:erasedups
alias cbc=". ~/cbc-1.0/bin/cbc"
alias vpip=~/venv/bin/pip
alias vpy="~/venv/bin/python"
alias vpy3="~/venv3.6/bin/python"
alias vi=vim
alias fk=fuck
alias la="ls -latr"
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
eval "$(thefuck --alias)"

function exists { which $1 &> /dev/null }

if exists percol; then
    function percol_select_history() {
        local tac
        exists gtac && tac="gtac" || { exists tac && tac="tac" || { tac="tail -r" } }
        BUFFER=$(fc -l -n 1 | eval $tac | percol --query "$LBUFFER")
        CURSOR=$#BUFFER
        zle -R -c
    }

    zle -N percol_select_history
    bindkey '^R' percol_select_history
fi

function exists_dir { ls -d */ &> /dev/null }

function cdp {
    # if exists_dir; then
    #     cd $(ls -Al | grep ^d | awk '{ print $NF}'| percol)
    #     cdp
    # else
    #     ls -A
    # fi
    if [ ! -z "$1" ]; then
        cd $1
    fi
    n=$(ls -Al | grep ^d | wc | awk '{ print $1 }')
    if [ $n -gt 0 ]; then
        # cd $(ls -al | percol)
        dir=$(ls -al | grep ^d | awk '{ print $NF}'| percol)
        if [ ! -z "$dir" ]; then
            cd $dir
            cdp
        fi
    else
        ls -A
    fi
}

function vip {
    _pwd=$(pwd)
    while :
    do
        x=$(ls -a | percol)
        if [ -z $x ]; then
            break
        elif [ -d $x ]; then
            cd $x
        elif [ -f $x ]; then
            vim $x
            break
        else
            echo "else"
            break
        fi
    done
    cd $_pwd
}

# OPAM configuration
. ~/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# z
. ~/z/z.sh
# nao sdk
export GTAGSLABEL=pygments
export PYTHONPATH=${PYTHONPATH}:/Users/mac/pynaoqi-python2.7-2.5.5.5-mac64/lib/python2.7/site-packages
export DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:/Users/mac/pynaoqi-python2.7-2.5.5.5-mac64/lib

