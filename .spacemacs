;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load. If it is the symbol `all' instead
   ;; of a list then all discovered layers will be installed.
   dotspacemacs-configuration-layers
   '(
     go
     erlang
     ;; javascript
     graphviz
     csv
     markdown
     lua
     nginx
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     (auto-completion :variables
                      auto-completion-enable-snippets-in-popup nil
                      auto-completion-tab-key-behavior 'complete
                      auto-completion-enable-sort-by-usage t
                      )
     osx
     better-defaults
     yaml
     emacs-lisp
     ;; common-lisp
     python
     ;; java
     ;; asm
     ;; (c-c++ :variables
     ;;       c-c++-enable-clang-support t)
     (clojure :variables
             clojure-enable-fancify-symbols t)
     ;; rust
     ;; (go :variables
     ;;     go-tab-width 4)
     (haskell :variables
              haskell-process-type 'stack-ghci
              haskell-completion-backend 'intero
              haskell-enable-hindent-style "johan-tibell")
     ;; racket
     ;; extra-langs
     ;;scheme
     ;;ocaml
     ;;elixir
     ;; react
     ;; ruby
     ;; ruby-on-rails
     ;; javascript
     ;; html
     tabbar
     ;; git
     ;; markdown
     org
     ;; (shell :variables
     ;;        shell-default-height 30
     ;;        shell-default-position 'bottom)
     ;; spell-checking
     syntax-checking
     ;; semantic
     ;; gtags
     ;; version-control
     )
   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages '(epc keyfreq paredit)
   ;; A list of packages and/or extensions that will not be install and loaded.
   dotspacemacs-excluded-packages '()
   ;; If non-nil spacemacs will delete any orphan packages, i.e. packages that
   ;; are declared in a layer which is not a member of
   ;; the list `dotspacemacs-configuration-layers'. (default t)
   dotspacemacs-delete-orphan-packages t))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 5
   ;; If non nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. (default t)
   dotspacemacs-check-for-update t
   ;; One of `vim', `emacs' or `hybrid'. Evil is always enabled but if the
   ;; variable is `emacs' then the `holy-mode' is enabled at startup. `hybrid'
   ;; uses emacs key bindings for vim's insert mode, but otherwise leaves evil
   ;; unchanged. (default 'vim)
   dotspacemacs-editing-style 'hybrid
   ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'random
   ;; List of items to show in the startup buffer. If nil it is disabled.
   ;; Possible values are: `recents' `bookmarks' `projects'.
   ;; (default '(recents projects))
   dotspacemacs-startup-lists '(recents projects)
   ;; Number of recent files to show in the startup buffer. Ignored if
   ;; `dotspacemacs-startup-lists' doesn't include `recents'. (default 5)
   dotspacemacs-startup-recent-list-size 5
   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'emacs-lisp-mode
   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-light
                         spacemacs-dark
                         solarized-light
                         solarized-dark
                         leuven
                         monokai
                         zenburn)
   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font. `powerline-scale' allows to quickly tweak the mode-line
   ;; size to make separators look not too crappy.
   dotspacemacs-default-font '("monaco"
                               :size 14
                               :weight normal
                               :width normal
                               :powerline-scale 1.1)
   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m)
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs C-i, TAB and C-m, RET.
   ;; Setting it to a non-nil value, allows for separate commands under <C-i>
   ;; and TAB or <C-m> and RET.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil
   ;; (Not implemented) dotspacemacs-distinguish-gui-ret nil
   ;; The command key used for Evil commands (ex-commands) and
   ;; Emacs commands (M-x).
   ;; By default the command key is `:' so ex-commands are executed like in Vim
   ;; with `:' and Emacs commands are executed with `<leader> :'.
   dotspacemacs-command-key ":"
   ;; If non nil `Y' is remapped to `y$'. (default t)
   dotspacemacs-remap-Y-to-y$ t
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil
   ;; If non nil then the last auto saved layouts are resume automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil
   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache
   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non nil then `ido' replaces `helm' for some commands. For now only
   ;; `find-files' (SPC f f), `find-spacemacs-file' (SPC f e s), and
   ;; `find-contrib-file' (SPC f e c) are replaced. (default nil)
   dotspacemacs-use-ido nil
   ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize nil
   ;; if non nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header nil
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   dotspacemacs-enable-paste-micro-state nil
   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4
   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom
   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t
   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil
   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90
   ;; If non nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols t
   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters the
   ;; point when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t
   ;; If non nil line numbers are turned on in all `prog-mode' and `text-mode'
   ;; derivatives. If set to `relative', also turns on relative line numbers.
   ;; (default nil)
   dotspacemacs-line-numbers 'relative
   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non nil advises quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
   ;; (default '("ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now. (default nil)
   dotspacemacs-default-package-repository nil
   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed'to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'all
   ))

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init', before layer configuration
executes.
 This function is mostly useful for variables that need to be set
before packages are loaded. If you are unsure, you should try in setting them in
`dotspacemacs/user-config' first."
  )

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,
you should place your code here."
  (setq debug-on-error nil)
  (evil-leader/set-key "SPC" 'avy-goto-word-or-subword-1)
  (define-key evil-hybrid-state-map (kbd "C-j") (kbd "C-e RET"))
  (define-key evil-hybrid-state-map (kbd "C-h") 'delete-backward-char)
  (define-key evil-hybrid-state-map (kbd "C-'") (kbd "`"))
  (define-key evil-normal-state-map (kbd "C-p") 'flycheck-previous-error)
  (define-key evil-normal-state-map (kbd "C-n") 'flycheck-next-error)
  (define-key evil-normal-state-map (kbd "`") 'evil-goto-mark-line)
  (define-key evil-normal-state-map (kbd "'") 'evil-goto-mark)
  (define-key evil-hybrid-state-map (kbd "C-u") (kbd "C-a C-k"))
  (define-key evil-hybrid-state-map (kbd "s-/") (kbd "M-/"))
  (define-key evil-normal-state-map (kbd "C-/") 'spacemacs/comment-or-uncomment-lines)
  (define-key evil-normal-state-map (kbd "0") (kbd "^"))
  (define-key evil-normal-state-map (kbd "-") (kbd "$"))
  (define-key minibuffer-local-map (kbd "C-h") 'delete-backward-char)
  (setq-default evil-escape-key-sequence "jk")
  (setq-default evil-escape-delay 0.5)
  ;; (setq truncate-lines t)
  (spacemacs/toggle-vi-tilde-fringe-off)
  ;; (spacemacs/toggle-golden-ratio-on)
  (spacemacs/toggle-indent-guide-globally-on)
  (add-to-list 'exec-path "~/.local/bin")
  (add-to-list 'exec-path "~/racket/bin")
  ;; (setq eclim-eclipse-dirs "/home/san/eclipse"
  ;;       eclim-executable "/home/san/eclipse/eclim")
  ;; (custom-set-variables '(scheme-program-name "petite"))
  ;; (setq scheme-program-name "petite")
  (modify-syntax-entry ?_ "w" (standard-syntax-table))
  (with-eval-after-load 'python
    (pyvenv-activate "~/venv")
    (spacemacs/set-leader-keys-for-major-mode 'python-mode (kbd "cc") (lambda () (interactive) (save-buffer) (spacemacs/python-execute-file nil)))
    (define-key evil-hybrid-state-map (kbd "C-h") 'python-indent-dedent-line-backspace)
    (define-key python-mode-map (kbd "C-;") 'yapfify-buffer)
    (define-key python-mode-map (kbd "C-'") (lambda () (interactive) (end-of-line) (insert "  # yapf: disable"))))
  ;; (with-eval-after-load 'go
  (spacemacs/set-leader-keys-for-major-mode 'go-mode (kbd "xx") (lambda () (interactive) (when (buffer-modified-p) (save-buffer)) (spacemacs/go-run-main)))
  (spacemacs/set-leader-keys-for-major-mode 'go-mode (kbd "gb") 'spacemacs/go-back)
  (spacemacs/set-leader-keys-for-major-mode 'emacs-lisp-mode (kbd "gb") 'spacemacs/go-back)
  (global-set-key (kbd "M-<right>") 'spacemacs/jump-to-definition)
  (global-set-key (kbd "M-<left>") 'spacemacs/go-back)
  (global-set-key (kbd "M-l") 'spacemacs/jump-to-definition)
  (global-set-key (kbd "M-h") 'spacemacs/go-back)
  ;; (global-set-key (kbd "<double-mouse-1>") 'spacemacs/jump-to-definition)
  ;; (global-set-key (kbd "<double-mouse-3>") 'spacemacs/go-back)
  ;; (global-set-key (kbd "<C-down-mouse-1>") 'spacemacs/jump-to-definition)
  ;; (global-set-key (kbd "M-<left>") 'spacemacs/go-back)
    ;; (define-key go-mode-map (kbd ", x a") (lambda () (interactive) (save-buffer) (spacemacs/go-run-main))))

  ;; (setq python-shell-interpreter "python2")
  (setenv "PYTHONPATH" (concat (getenv "PYTHONPATH") ":" "/Users/mac/pynaoqi-python2.7-2.5.5.5-mac64/lib/python2.7/site-packages"))
  (setenv "DYLD_LIBRARY_PATH" (concat (getenv "DYLD_LIBRARY_PATH") ":" "/Users/mac/pynaoqi-python2.7-2.5.5.5-mac64/lib"))
  ;; (setenv "http_proxy" "http://127.0.0.1:1087")
  ;; (setenv "https_proxy" "http://127.0.0.1:1087")
  ;; export PYTHONPATH=${PYTHONPATH}:/Users/mac/pynaoqi-python2.7-2.5.5.5-mac64/lib/python2.7/site-packages
  ;; export DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:/Users/mac/pynaoqi-python2.7-2.5.5.5-mac64/lib

  (setq mac-option-modifier 'super)
  (setq mac-command-modifier 'meta)

  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1)
  ;; (with-eval-after-load 'js
  ;;   (define-key js2-mode-map (kbd "C-;") 'web-beautify-js))
  (with-eval-after-load 'company
    (define-key company-active-map (kbd "C-h") 'delete-backward-char)
    (define-key company-active-map (kbd "C-w") 'evil-delete-backward-word)
    (define-key company-active-map (kbd "C-f") nil))
  (with-eval-after-load 'company-template
    (define-key company-template-nav-map (kbd "C-;") 'company-template-forward-field)
    (define-key company-template-nav-map (kbd "<tab>") 'company-complete-selection)
    (define-key company-template-nav-map (kbd "TAB") 'company-complete-selection))
  (setq company-show-numbers t)
  (with-eval-after-load 'yasnippet
    ;; (smartparens-mode)
    (define-key yas-keymap (kbd "C-;") 'yas-next-field)
    (define-key yas-keymap (kbd "<tab>") nil)
    (define-key yas-keymap (kbd "TAB") nil))

  (dotimes (i 9)
    (let ((n (+ i 1)))
      (eval `(defun ,(intern (format "tabbar-select-tab-%s" n)) (&optional arg)
               ,(format "Select the tab with number %i." n)
               (interactive "P")
               (tabbar-select-tab-by-number ,i)))))
  (dotimes (i 9)
    (eval `(spacemacs/set-leader-keys (format "%s" (+ i 1)) (intern-soft (format "tabbar-select-tab-%s" (+ i 1))))))
  (spacemacs/set-leader-keys (kbd "C-p") 'tabbar-backward-tab)
  (spacemacs/set-leader-keys (kbd "C-n") 'tabbar-forward-tab)

  (spacemacs/set-leader-keys (kbd "o") (kbd "C-x o"))
  (spacemacs/set-leader-keys (kbd "hs") 'helm-semantic-or-imenu)

  (with-eval-after-load 'helm
    (define-key helm-map (kbd "C-h") 'delete-backward-char)
    (define-key helm-map (kbd "C-w") 'evil-delete-backward-word))
  (with-eval-after-load 'utop
    (define-key utop-mode-map (kbd "C-n") 'utop-history-goto-next)
    (define-key utop-mode-map (kbd "C-p") 'utop-history-goto-prev))
  (spacemacs/set-leader-keys (kbd "fn") (lambda () (interactive) (message "%S" buffer-file-name)))

  ;; (define-key org-mode-map (kbd "<drag-n-drop>") (lambda () (interactive) (insert "aaaa")))
  ;; not work yasnippet with smartparens
  ;; (add-to-list 'yas-before-expand-snippet-hook 'spacemacs/toggle-smartparens-on)
  ;; (add-to-list 'yas-after-exit-snippet-hook 'spacemacs/toggle-smartparens-off)
  (add-hook 'go-mode-hook (lambda () (interactive) (flycheck-mode 0)))
  (add-hook 'org-mode-hook 'smartparens-mode)
  (add-hook 'hack-local-variables-hook (lambda () (setq truncate-lines t)))
  (add-hook 'emacs-lisp-mode-hook 'paredit-mode)
  ;; (add-hook 'emacs-lisp-mode-hook (lambda ()
  ;;                                   (modify-syntax-entry ?_ "w")
  ;;                                   (define-key emacs-lisp-mode-map (kbd "C-l")
  ;;                                     (lambda ()
  ;;                                       (interactive)
  ;;                                       (insert "lambda ()")
  ;;                                       (backward-char 1)))))
  (add-hook 'python-mode-hook (lambda ()
                                (if (not (file-exists-p (buffer-file-name)))
                                    (progn
                                      (beginning-of-buffer)
                                      (insert "# -*- coding: utf-8 -*-\n\n")))))

  (add-hook 'racket-mode-hook 'paredit-mode)
  (add-hook 'racket-mode-hook
            (lambda ()
              (define-key racket-mode-map (kbd "C-M-l")
                (lambda ()
                  (interactive)
                  (insert "λ ()")
                  (backward-char 1)))))

  ;; (add-to-list 'auto-mode-alist '("\\.rkt[dl]?\\'" . scheme-mode))
  (add-to-list 'auto-mode-alist '("\\.hdl\\'" . text-mode))
  (with-eval-after-load 'scheme
    (spacemacs|define-jump-handlers scheme-mode racket-visit-definition))
  (add-to-list 'load-path (expand-file-name "private/extension" user-emacs-directory))
  ;; (autoload 'py-refactor-mode "py-refactor" "Enter py-refactor-mode." t)
  ;; (add-hook 'python-mode-hook 'py-refactor-mode)
  ;; (spacemacs|define-custom-layout "refactor"
  ;;   :binding "r"
  ;;   :body
  ;;   (find-file-existing "~/.emacs.d/private/extension/refactor_server.py")
  ;;   (split-window-below)
  ;;   ;; (evil-window-down 1)
  ;;   (find-file-existing "~/.emacs.d/private/extension/py-refactor.el"))


  (add-hook 'racket-mode-hook 'tabbar-local-mode)
  (add-hook 'intero-repl-mode-hook 'tabbar-local-mode)
  ;; (add-hook 'inferior-python-mode-hook 'tabbar-local-mode)

  (with-eval-after-load 'haskell
    (define-key haskell-mode-map (kbd "C-'") (kbd "`")))

  (with-eval-after-load 'haskell
    ;; (define-key haskell-mode-map (kbd "C-h") 'delete-backward-char)
    (spacemacs/set-leader-keys-for-major-mode 'haskell-mode (kbd "f") 'hindent-reformat-buffer)
    (add-hook 'before-save-hook 'haskell-mode-stylish-buffer nil t))
  (defun haskell-indentation-advice ()
    (when (and (< 1 (line-number-at-pos))
               (save-excursion
                 (forward-line -1)
                 (string= "" (s-trim (buffer-substring (line-beginning-position) (line-end-position))))))
      (delete-region (line-beginning-position) (point))))

  (advice-add 'haskell-indentation-newline-and-indent
              :after 'haskell-indentation-advice)

  ;; asm
  ;; (setq x86-lookup-pdf "~/325383-sdm-vol-2abcd.pdf")

  )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (queue clojure-mode clojure-snippets clj-refactor inflections edn multiple-cursors peg cider-eval-sexp-fu cider go-guru go-eldoc company-go go-mode erlang web-beautify simple-httpd json-mode json-snatcher json-reformat js2-mode js-doc company-tern dash-functional tern coffee-mode intero hlint-refactor hindent helm-hoogle haskell-snippets flycheck-haskell company-ghci company-ghc ghc haskell-mode company-cabal cmm-mode diminish smartparens evil helm helm-core avy async graphviz-dot-mode csv-mode mmm-mode markdown-toc markdown-mode gh-md lua-mode tabbar yapfify yaml-mode pyvenv pytest pyenv-mode py-isort pip-requirements paredit org-projectile org-present org-pomodoro alert log4e gntp org-download nginx-mode live-py-mode keyfreq hy-mode htmlize helm-pydoc helm-company helm-c-yasnippet gnuplot fuzzy flycheck-pos-tip pos-tip flycheck epc ctable concurrent deferred define-word cython-mode company-statistics company-anaconda company auto-yasnippet yasnippet anaconda-mode pythonic ac-ispell auto-complete ws-butler winum which-key volatile-highlights vi-tilde-fringe uuidgen use-package unfill toc-org spaceline reveal-in-osx-finder restart-emacs request rainbow-delimiters popwin persp-mode pcre2el pbcopy paradox osx-trash osx-dictionary org-plus-contrib org-bullets open-junk-file neotree mwim move-text macrostep lorem-ipsum linum-relative link-hint launchctl info+ indent-guide hungry-delete hl-todo highlight-parentheses highlight-numbers highlight-indentation hide-comnt help-fns+ helm-themes helm-swoop helm-projectile helm-mode-manager helm-make helm-flx helm-descbinds helm-ag google-translate golden-ratio flx-ido fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-ediff evil-args evil-anzu eval-sexp-fu elisp-slime-nav dumb-jump column-enforce-mode clean-aindent-mode auto-highlight-symbol auto-compile aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line)))
 '(tabbar-separator (quote (0.5))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
